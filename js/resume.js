$('document').ready(function() {
	//Hide the possibly outdated resumes.
	$("#showResume").click(function() {
		if ($("#resumeDownloads").is(":visible")) {
			//Hide if they are already showing
			$("#resumeDownloads").hide("slow");
		}
		else {
			//Otherwise, show the links.
			$("#resumeDownloads").show("slow");
		}
	});
})